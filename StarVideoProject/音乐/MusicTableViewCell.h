//
//  MusicTableViewCell.h
//  StarVideoProject
//
//  Created by lijiang on 2018/12/2.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "MusicModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MusicTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *intruduceLabel;

@property (strong, nonatomic) MusicModel *model;
@end

NS_ASSUME_NONNULL_END
