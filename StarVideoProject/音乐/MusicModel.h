//
//  MusicModel.h
//  StarVideoProject
//
//  Created by lijiang on 2018/12/2.
//  Copyright © 2018年 star. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MusicModel : NSObject
@property (copy,nonatomic) NSString *name;
@property (copy,nonatomic) NSString *intruduce;
@end

NS_ASSUME_NONNULL_END
