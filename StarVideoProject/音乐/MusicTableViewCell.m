//
//  MusicTableViewCell.m
//  StarVideoProject
//
//  Created by lijiang on 2018/12/2.
//  Copyright © 2018年 star. All rights reserved.
//

#import "MusicTableViewCell.h"

@implementation MusicTableViewCell


- (void)setModel:(MusicModel *)model {
    _model = model;
    
    _nameLabel.text = model.name;
    _intruduceLabel.text = model.intruduce;
}
@end
