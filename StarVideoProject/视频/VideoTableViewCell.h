//
//  VideoTableViewCell.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/30.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "WMPlayer.h"
#import "VideoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface VideoTableViewCell : BaseTableViewCell 
@property (weak, nonatomic) IBOutlet UIImageView *starImageView;
@property (weak, nonatomic) IBOutlet UIImageView *playImageView;
@property (weak, nonatomic) IBOutlet WMPlayer *wmPlayerView;

@property (strong, nonatomic) VideoModel *model;
@end

NS_ASSUME_NONNULL_END
