//
//  VideoTableViewController.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/30.
//  Copyright © 2018年 star. All rights reserved.
//

#import "VideoTableViewController.h"
#import "VideoTableViewCell.h"
@interface VideoTableViewController ()<UITableViewDelegate,UITableViewDataSource,WMPlayerDelegate>

@property (copy, nonatomic) NSArray *dataArray;
@end
@implementation VideoTableViewController
static NSString *cellID = @"VideoTableViewCell";
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initial];
    
}

- (void)initial {

    WMPlayerModel *model = [WMPlayerModel new];
    model.videoURL = [NSURL URLWithString:@"http://111.202.85.141/vlive.qqvideo.tc.qq.com/Atrx1GP3ty8Xf67Svy6GZJ-ewttLyDGWoEDFI06OVHO8/a020097ly6l.p201.1.mp4?level=0&fmt=shd&sdtfrom=&platform=10901&vkey=3E6F1BD676D5ECF6801BB31FC6B71630C45AFACD7EBAE06782556DFDC8E7CE3E439A25250297B2393F4B5B5176F7E590F2D17793A28F74E8EAE5AAECFBABDA947810754ECDC77B0394E41B02583CC0359D206CC21219AC2AFF13F705F012E80873E5C39071969DA9BE2F0BB11FE24ED43C2A42F992E0D344&locid=bbf12237-b00d-4537-a01f-4ef00327bea7&size=3774984&ocid=2507609516"];
//    model.title = @"dsds";
    _dataArray = @[model,model];
    
}

#pragma mark -UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    cell.model = _dataArray[indexPath.row];
    cell.wmPlayerView.delegate = self;
    [cell.wmPlayerView play];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 200;
}

#pragma mark - WMPlayerDelegate

- (void)wmplayer:(WMPlayer *)wmplayer clickedFullScreenButton:(UIButton *)fullScreenBtn {
    
    if (fullScreenBtn.selected) {
        WMPlayer *player = [[WMPlayer alloc]initPlayerModel:wmplayer.playerModel];
        player.tag = 444;
        player.frame = [UIScreen mainScreen].bounds;
        [[UIApplication sharedApplication].keyWindow addSubview:player];
    }else {
        WMPlayer *player = [[UIApplication sharedApplication].keyWindow viewWithTag:444];
        if (player) {
            [player removeFromSuperview];
        }
        
    }
    
    
}

@end
