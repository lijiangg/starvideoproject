//
//  VideoTableViewCell.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/30.
//  Copyright © 2018年 star. All rights reserved.
//

#import "VideoTableViewCell.h"

@implementation VideoTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    _wmPlayerView.backBtnStyle = BackBtnStyleNone;

}

- (void)setModel:(VideoModel *)model {
    _model = model;
    _wmPlayerView.playerModel = model;
}



@end
