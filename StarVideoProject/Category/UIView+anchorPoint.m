//
//  UIView+anchorPoint.m
//  FTMS_MIVS
//
//  Created by lijiang on 2018/11/19.
//  Copyright © 2018年 yuanjingshidian. All rights reserved.
//

#import "UIView+anchorPoint.h"

@implementation UIView (anchorPoint)

- (void)changeAnchorPoint:(CGPoint)anchorPoint
{
    CGPoint oldOrigin = self.frame.origin;
    self.layer.anchorPoint = anchorPoint;
    CGPoint newOrigin = self.frame.origin;
    
    CGPoint transition;
    transition.x = newOrigin.x - oldOrigin.x;
    transition.y = newOrigin.y - oldOrigin.y;
    
    self.center = CGPointMake (self.center.x - transition.x, self.center.y - transition.y);
}

- (void)setDefaultAnchorPoint
{
    [self changeAnchorPoint:CGPointMake(0.5f, 0.5f)];
}

@end
