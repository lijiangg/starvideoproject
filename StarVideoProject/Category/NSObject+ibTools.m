//
//  NSObject+ibTools.m
//  FTMS_MIVS
//
//  Created by lijiang on 2018/11/16.
//  Copyright © 2018年 yuanjingshidian. All rights reserved.
//

#import "NSObject+ibTools.h"
#import <UIKit/UIKit.h>
@implementation NSObject (ibTools)

- (UIViewController *)getVcWithSbname:(NSString *)sbname ID:(NSString *)ID {
    
    return [[UIStoryboard storyboardWithName:sbname bundle:nil] instantiateViewControllerWithIdentifier:ID];
}

@end
