//
//  UIView+ibTools.m
//  FTMS_MIVS
//
//  Created by lijiang on 2018/11/16.
//  Copyright © 2018年 yuanjingshidian. All rights reserved.
//

#import "UIView+ibTools.h"

@implementation UIView (ibTools)

- (void)setBorderColor:(UIColor *)borderColor {
    self.layer.borderColor = borderColor.CGColor;
}

- (UIColor *)borderColor {
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

- (void)setBorderWidth:(CGFloat)borderWidth {
    self.layer.borderWidth = borderWidth;
}

- (CGFloat)borderWidth {
    return self.layer.borderWidth;
}

- (void)setLayerCorner:(CGFloat)layerCorner {
    self.layer.cornerRadius = layerCorner;
}

- (CGFloat)layerCorner {
    return self.layer.cornerRadius;
}




@end
