//
//  UIColor+category.h
//  M_FTMS
//
//  Created by lijiang on 2018/11/21.
//  Copyright © 2018年 yuanjingshidian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (category)
- (UIImage *)getImage;
+ (UIColor *)colorFromHexString:(NSString*)colorString;
@end

NS_ASSUME_NONNULL_END
