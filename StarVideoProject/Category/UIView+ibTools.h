//
//  UIView+ibTools.h
//  FTMS_MIVS
//
//  Created by lijiang on 2018/11/16.
//  Copyright © 2018年 yuanjingshidian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (ibTools)

@property (assign, nonatomic)IBInspectable CGFloat borderWidth,layerCorner;
@property (strong, nonatomic)IBInspectable UIColor *borderColor;

@end

NS_ASSUME_NONNULL_END
