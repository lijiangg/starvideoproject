//
//  NSObject+ibTools.h
//  FTMS_MIVS
//
//  Created by lijiang on 2018/11/16.
//  Copyright © 2018年 yuanjingshidian. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (ibTools)
- (UIViewController *)getVcWithSbname:(NSString *)sbname ID:(NSString *)ID;
@end

NS_ASSUME_NONNULL_END
