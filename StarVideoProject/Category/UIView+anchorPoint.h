//
//  UIView+anchorPoint.h
//  FTMS_MIVS
//
//  Created by lijiang on 2018/11/19.
//  Copyright © 2018年 yuanjingshidian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (anchorPoint)
- (void)setDefaultAnchorPoint;
- (void)changeAnchorPoint:(CGPoint)anchorPoint;
@end

NS_ASSUME_NONNULL_END
