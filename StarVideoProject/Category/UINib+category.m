//
//  UINib+category.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import "UINib+category.h"

@implementation UINib (category)

+ (id)nibWithNibName:(NSString *)nibName target:(id)target {
    
    return [[UINib nibWithNibName:nibName bundle:nil]instantiateWithOwner:target options:nil].firstObject;
}

@end
