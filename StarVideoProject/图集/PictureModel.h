//
//  PictureModel.h
//  StarVideoProject
//
//  Created by lijiang on 2018/12/3.
//  Copyright © 2018年 star. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PictureModel : NSObject

@property (copy, nonatomic) NSString *imagePath;


@end

NS_ASSUME_NONNULL_END
