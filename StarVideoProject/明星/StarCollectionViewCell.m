//
//  StarCollectionViewCell.m
//  StarVideoProject
//
//  Created by lijiang on 2018/12/2.
//  Copyright © 2018年 star. All rights reserved.
//

#import "StarCollectionViewCell.h"
#import <UIImageView+WebCache.h>
@implementation StarCollectionViewCell

- (void)setModel:(StarModel *)model {
    _model = model;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.imagePath] placeholderImage:[UIImage imageNamed:placeholderImageName]];
}
@end
