//
//  StarCollectionViewController.h
//  StarVideoProject
//
//  Created by lijiang on 2018/12/2.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseCollectionViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface StarCollectionViewController : BaseCollectionViewController

@end

NS_ASSUME_NONNULL_END
