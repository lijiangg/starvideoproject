//
//  StarCollectionViewCell.h
//  StarVideoProject
//
//  Created by lijiang on 2018/12/2.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseCollectionViewCell.h"
#import "StarModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface StarCollectionViewCell : BaseCollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) StarModel *model;
@end

NS_ASSUME_NONNULL_END
