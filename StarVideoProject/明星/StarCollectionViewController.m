//
//  StarCollectionViewController.m
//  StarVideoProject
//
//  Created by lijiang on 2018/12/2.
//  Copyright © 2018年 star. All rights reserved.
//

#import "StarCollectionViewController.h"
#import "StarCollectionViewCell.h"
@interface StarCollectionViewController ()
@property (copy, nonatomic) NSArray *dataArray;
@end
@implementation StarCollectionViewController

static NSString *cellID = @"StarCollectionViewCell";
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setSubviews];
    [self initial];
}

- (void)setSubviews {
    UICollectionViewFlowLayout *layout =(UICollectionViewFlowLayout *) self.collectionView.collectionViewLayout;
    CGFloat itemWidth = self.view.bounds.size.width/3 - 10;
    layout.itemSize = CGSizeMake(itemWidth, itemWidth);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 0;
}

- (void)initial {
    StarModel *model = [StarModel new];
    model.imagePath = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1543169405820&di=d9b6ab42e3da3971b0eba275c8b18a99&imgtype=0&src=http%3A%2F%2Fimg5.duitang.com%2Fuploads%2Fitem%2F201507%2F21%2F20150721211815_5NQvy.jpeg";
    _dataArray = @[model,model,model,model,model,model,model,model,model,model,model,model];
}



- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    StarCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    cell.model = _dataArray[indexPath.row];
    return cell;
}

@end
