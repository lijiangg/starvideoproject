//
//  RecommendedPictureCollectionViewCell.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/26.
//  Copyright © 2018年 star. All rights reserved.
//

#import "RecommendedPictureCollectionViewCell.h"
#import <UIImageView+WebCache.h>
@implementation RecommendedPictureCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)setModel:(RecommendedPictureModel *)model {
    _model = model;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.imagePath] placeholderImage:[UIImage imageNamed:placeholderImageName]];
}


@end
