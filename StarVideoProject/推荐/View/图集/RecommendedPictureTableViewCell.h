//
//  RecommendedPictureTableViewCell.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/27.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "RecommendedPictureView.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecommendedPictureTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet RecommendedPictureView *pictureView;

@end

NS_ASSUME_NONNULL_END
