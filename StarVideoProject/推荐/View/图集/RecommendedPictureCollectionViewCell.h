//
//  RecommendedPictureCollectionViewCell.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/26.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseCollectionViewCell.h"
#import "RecommendedPictureModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecommendedPictureCollectionViewCell : BaseCollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (copy, nonatomic) RecommendedPictureModel *model;
@end

NS_ASSUME_NONNULL_END
