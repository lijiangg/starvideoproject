//
//  RecommendedScroHeadView.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScroImageView.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecommendedScroHeadView : UITableViewHeaderFooterView
@property (weak, nonatomic) IBOutlet ScroImageView *scroImageView;

@end

NS_ASSUME_NONNULL_END
