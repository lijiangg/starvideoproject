//
//  RecommendedStarTableViewCell.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/27.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "RecommendedStarView.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecommendedStarTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet RecommendedStarView *statView;

@end

NS_ASSUME_NONNULL_END
