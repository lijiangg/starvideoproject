//
//  RecommendedStarView.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/27.
//  Copyright © 2018年 star. All rights reserved.
//

#import "RecommendedStarView.h"

@implementation RecommendedStarView

static NSString *cellID = @"RecommendedStarCollectionViewCell";
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _view = [UINib nibWithNibName:@"RecommendedStarView" target:self];
        [self addSubview:_view];
        [_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        
        
        
        [_collectionView registerNib:[UINib nibWithNibName:@"RecommendedStarCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:cellID];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    UICollectionViewFlowLayout *flowlayout = [UICollectionViewFlowLayout new];
    CGFloat inset = 5;
    flowlayout.itemSize = CGSizeMake((self.frame.size.width - inset * 2)/3, (self.frame.size.height - inset)/2);
    flowlayout.minimumLineSpacing = inset;
    flowlayout.minimumInteritemSpacing = inset;
    flowlayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    _collectionView.collectionViewLayout = flowlayout;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    RecommendedStarCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellID forIndexPath:indexPath];
    cell.model = _dataArray[indexPath.row];
    return cell;
}
@end
