//
//  RecommendedStarCollectionViewCell.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/27.
//  Copyright © 2018年 star. All rights reserved.
//

#import "RecommendedStarCollectionViewCell.h"
#import <UIImageView+WebCache.h>
@implementation RecommendedStarCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(RecommendedStarModel *)model {
    _model = model;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.imagePath] placeholderImage:[UIImage imageNamed:placeholderImageName]];
}
@end
