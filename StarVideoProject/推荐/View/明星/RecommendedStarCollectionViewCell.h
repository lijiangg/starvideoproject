//
//  RecommendedStarCollectionViewCell.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/27.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseCollectionViewCell.h"
#import "RecommendedStarModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecommendedStarCollectionViewCell : BaseCollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (copy, nonatomic) RecommendedStarModel *model;
@end

NS_ASSUME_NONNULL_END
