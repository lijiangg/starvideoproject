//
//  RecommendedStarView.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/27.
//  Copyright © 2018年 star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionView.h"
#import "RecommendedStarCollectionViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecommendedStarView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet BaseCollectionView *collectionView;

@property (copy, nonatomic) NSArray *dataArray;
@end

NS_ASSUME_NONNULL_END
