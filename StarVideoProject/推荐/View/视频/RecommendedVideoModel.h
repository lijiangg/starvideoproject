//
//  RecommendedVideoModel.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/26.
//  Copyright © 2018年 star. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecommendedVideoModel : NSObject

@property (copy, nonatomic) NSString *ImagePath;
@property (copy, nonatomic) NSString *name;
@property (copy, nonatomic) NSString *introduce;
@end

NS_ASSUME_NONNULL_END
