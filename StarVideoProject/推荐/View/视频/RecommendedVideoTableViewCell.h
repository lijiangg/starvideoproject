//
//  RecommendedVideoTableViewCell.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/26.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "RecommendedVideoView.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecommendedVideoTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet RecommendedVideoView *videoView;

@end

NS_ASSUME_NONNULL_END
