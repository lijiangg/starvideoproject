//
//  RecommendedVideoCollectionViewCell.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/26.
//  Copyright © 2018年 star. All rights reserved.
//

#import "RecommendedVideoCollectionViewCell.h"
#import <UIImageView+WebCache.h>
@implementation RecommendedVideoCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(RecommendedVideoModel *)model {
    _model = model;
    [_imageView sd_setImageWithURL:[NSURL URLWithString:model.ImagePath] placeholderImage:[UIImage imageNamed:placeholderImageName]];
    _nameLabel.text = model.name;
    _describtionLabel.text = model.introduce;
    
}
@end
