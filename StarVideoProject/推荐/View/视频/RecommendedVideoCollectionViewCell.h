//
//  RecommendedVideoCollectionViewCell.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/26.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseCollectionViewCell.h"
#import "RecommendedVideoModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecommendedVideoCollectionViewCell : BaseCollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *describtionLabel;

@property (strong, nonatomic) RecommendedVideoModel *model;
@end

NS_ASSUME_NONNULL_END
