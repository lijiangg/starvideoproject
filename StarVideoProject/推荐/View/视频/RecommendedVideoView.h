//
//  RecommendedVideoView.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/26.
//  Copyright © 2018年 star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseCollectionView.h"
#import "RecommendedVideoCollectionViewCell.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecommendedVideoView : UIView <UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet BaseCollectionView *collectionView;

@property (copy, nonatomic) NSArray <RecommendedVideoModel *>*dataArray;
@end

NS_ASSUME_NONNULL_END
