//
//  ScroImageView.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import "ScroImageView.h"
#import <UIButton+WebCache.h>

#define imageButtonTag 223232323
@implementation ScroImageView {
    CGRect _frame;
    void (^getFrame)(CGRect);
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _view = [UINib nibWithNibName:@"ScroImageView" target:self];
        [self addSubview:_view];
        [_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    _frame = rect;
    getFrame(rect);
}

- (void)setImageArray:(NSArray *)imageArray {
    _imageArray = imageArray;
 
}

- (void)reLoadImageViewButton {
    if (_frame.size.height == 0) {
        WeakSelf;
        __strong ScroImageView *strongSelf = weakSelf;
        strongSelf -> getFrame = ^(CGRect frame){
            [self reSetSubView];
        };
    }else{
        [self reSetSubView];
    }
   
    
}

- (void)reSetSubView {
    //清除上次添加的view
    for (UIView *view in _scroView.subviews) {
        [view removeFromSuperview];
    }
    
    //添加新ImageView
    _scroView.contentSize = CGSizeMake(_frame.size.width * _imageArray.count, 0);
    for (int index = 0; index < _imageArray.count; index++) {
        CGFloat width = _frame.size.width;
        CGFloat height = _frame.size.height;
        CGFloat x = width * index;
        CGFloat y = 0;
        NSString *imagePath = _imageArray[index];
        UIButton *imageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        imageButton.adjustsImageWhenHighlighted = NO;
        imageButton.frame = CGRectMake(x, y, width, height);
        if ([imagePath containsString:@"http://"] || [imagePath containsString:@"https://"]) {
            //网络
            [imageButton sd_setImageWithURL:[NSURL URLWithString:imagePath] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:placeholderImageName]];
        }else{
            //本地
            [imageButton setImage:[UIImage imageNamed:imagePath] forState:UIControlStateNormal];
        }
        [imageButton addTarget:self action:@selector(imageButtonOnClick:) forControlEvents:UIControlEventTouchUpInside];
        imageButton.contentMode = UIViewContentModeScaleAspectFill;
        imageButton.tag = imageButtonTag;
        [_scroView addSubview:imageButton];
        
    }
}

- (void)imageButtonOnClick:(UIButton *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(imageButtonOnClick:)]) {
        [_delegate imageButtonOnClick:sender.tag - imageButtonTag];
    }
}
@end
