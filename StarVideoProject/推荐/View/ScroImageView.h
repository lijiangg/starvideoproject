//
//  ScroImageView.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol ScroImageViewDelegate <NSObject>

- (void)imageButtonOnClick:(NSInteger)index;

@end
@interface ScroImageView : UIView
@property (strong, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIScrollView *scroView;

@property (weak, nonatomic)IBOutlet id<ScroImageViewDelegate> delegate;

@property (copy, nonatomic) NSArray *imageArray; //path or name

- (void)reLoadImageViewButton;//开始重绘Button
@end

NS_ASSUME_NONNULL_END
