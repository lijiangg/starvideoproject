//
//  RecommendedMusicTableViewCell.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import "RecommendedMusicTableViewCell.h"

@implementation RecommendedMusicTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(RecommendedMusicModel *)model {
    _model = model;
    _nameLabel.text = model.name;
    _intruduceLabel.text = model.introduce;
}
@end
