//
//  RecommendedMusicTableViewCell.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseTableViewCell.h"
#import "RecommendedMusicModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface RecommendedMusicTableViewCell : BaseTableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *intruduceLabel;

@property (strong, nonatomic) RecommendedMusicModel *model;
@end

NS_ASSUME_NONNULL_END
