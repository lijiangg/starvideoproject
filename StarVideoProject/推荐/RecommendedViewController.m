//
//  RecommendedViewController.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import "RecommendedViewController.h"
#import "SearchBarView.h"
#import "RecommendedScroHeadView.h"
#import "RecommendedHeaderView.h"
#import "RecommendedVideoTableViewCell.h"
#import "RecommendedMusicTableViewCell.h"
#import "RecommendedPictureTableViewCell.h"
#import "RecommendedStarTableViewCell.h"

@interface RecommendedViewController ()
@property (weak, nonatomic) IBOutlet SearchBarView *searchBarView;
@property (copy, nonatomic) NSArray *SectionTitleArray;
@property (copy, nonatomic) NSArray *videoDataArray;
@property (copy, nonatomic) NSArray *musicDataArray;
@property (copy, nonatomic) NSArray *pictureDataArray;
@property (copy, nonatomic) NSArray *starDataArray;

@end


@implementation RecommendedViewController
static NSString *scroHeaderID = @"RecommendedScroHeadView";
static NSString *headerID = @"RecommendedHeaderView";
static NSString *videoCellID = @"RecommendedVideoTableViewCell";
static NSString *musicCellID = @"RecommendedMusicTableViewCell";
static NSString *pictureCellID = @"RecommendedPictureTableViewCell";
static NSString *starCellID = @"RecommendedStarTableViewCell";
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setSubviews];
    [self initial];
}

- (void)initial {
    _SectionTitleArray = @[@"",@"视频",@"音乐",@"图集",@"明星"];
    
    RecommendedVideoModel *videomodel = [RecommendedVideoModel new];
    videomodel.ImagePath = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1543169405820&di=d9b6ab42e3da3971b0eba275c8b18a99&imgtype=0&src=http%3A%2F%2Fimg5.duitang.com%2Fuploads%2Fitem%2F201507%2F21%2F20150721211815_5NQvy.jpeg";
    videomodel.name = @"胡歌";
    videomodel.introduce = @"一名演员";
    _videoDataArray = @[videomodel,videomodel,videomodel];
    
    RecommendedMusicModel *musicmodel = [RecommendedMusicModel new];
    musicmodel.name = @"胡歌";
    musicmodel.introduce = @"一名演员";
    _musicDataArray = @[musicmodel,musicmodel,musicmodel];
    
    RecommendedPictureModel *picturemodel = [RecommendedPictureModel new];
    picturemodel.imagePath = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1543169405820&di=d9b6ab42e3da3971b0eba275c8b18a99&imgtype=0&src=http%3A%2F%2Fimg5.duitang.com%2Fuploads%2Fitem%2F201507%2F21%2F20150721211815_5NQvy.jpeg";
    _pictureDataArray = @[picturemodel,picturemodel,picturemodel,picturemodel,picturemodel,picturemodel,picturemodel,picturemodel,picturemodel,picturemodel,picturemodel,picturemodel,picturemodel];
    
    RecommendedStarModel *starmodel = [RecommendedStarModel new];
    starmodel.imagePath = @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1543169405820&di=d9b6ab42e3da3971b0eba275c8b18a99&imgtype=0&src=http%3A%2F%2Fimg5.duitang.com%2Fuploads%2Fitem%2F201507%2F21%2F20150721211815_5NQvy.jpeg";
    _starDataArray = @[starmodel,starmodel,starmodel,starmodel,starmodel,starmodel,starmodel,starmodel,starmodel,starmodel,starmodel,starmodel];
}

- (void)setSubviews {
    [self.tableView registerNib:[UINib nibWithNibName:@"RecommendedScroHeadView" bundle:nil] forHeaderFooterViewReuseIdentifier:scroHeaderID];
    [self.tableView registerNib:[UINib nibWithNibName:@"RecommendedHeaderView" bundle:nil] forHeaderFooterViewReuseIdentifier:headerID];
    
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 0;
            break;
        case 1:
            return 1;
            break;
        case 2:
            return _musicDataArray.count;
            break;
        case 3:
            return 1;
            break;
        case 4:
            return 1;
            break;
        default:
            return 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if  (indexPath.section == 1) {
        RecommendedVideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:videoCellID];
        cell.videoView.dataArray = _videoDataArray;
        return cell;
    }else if (indexPath.section == 2) {
        RecommendedMusicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:musicCellID];
        cell.model = _musicDataArray[indexPath.row];
        return cell;
    }else if (indexPath.section == 3){
        RecommendedPictureTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:pictureCellID];
        cell.pictureView.dataArray = _pictureDataArray;
        return cell;
    }else {
        RecommendedStarTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:starCellID];
        cell.statView.dataArray = _starDataArray;
        return cell;
    }
    

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        return 200;
    }else if (indexPath.section == 2) {
        return 50;
    }else if (indexPath.section == 3) {
        return 300;
    }else if (indexPath.section == 4) {
        return 300;
    }else{
        return 0;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        RecommendedScroHeadView *headerView = (RecommendedScroHeadView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:scroHeaderID];
        headerView.scroImageView.imageArray = @[@"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1543169405820&di=d9b6ab42e3da3971b0eba275c8b18a99&imgtype=0&src=http%3A%2F%2Fimg5.duitang.com%2Fuploads%2Fitem%2F201507%2F21%2F20150721211815_5NQvy.jpeg",
                                                @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1543169418507&di=cefa2a41750ace5d9d0f7b83bd7df4b9&imgtype=jpg&src=http%3A%2F%2Fimg2.imgtn.bdimg.com%2Fit%2Fu%3D942441842%2C1589958443%26fm%3D214%26gp%3D0.jpg",
                                                @"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1543169405819&di=482a02786ff95bc6a7c4bfd7fc7634c8&imgtype=0&src=http%3A%2F%2Fimg5.duitang.com%2Fuploads%2Fitem%2F201602%2F12%2F20160212180334_KWfXe.thumb.700_0.jpeg"];
        [headerView.scroImageView reLoadImageViewButton];
        return headerView;
    }else {
        RecommendedHeaderView *headerView = (RecommendedHeaderView *)[tableView dequeueReusableHeaderFooterViewWithIdentifier:headerID];
        headerView.contentView.backgroundColor = [UIColor whiteColor];
        headerView.titleLabel.text = _SectionTitleArray[section];
        return headerView;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 200;
    }else{
        return 80;
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
