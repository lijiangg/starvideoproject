//
//  SearchBarView.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/29.
//  Copyright © 2018年 star. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchBarView : UIView

@property (weak, nonatomic) IBOutlet UIButton *searButton;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@end

NS_ASSUME_NONNULL_END
