//
//  TabBarButtonView.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/27.
//  Copyright © 2018年 star. All rights reserved.
//

#import "TabBarButtonView.h"

@implementation TabBarButtonView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+ (TabBarButtonView *)bar {
   return [UINib nibWithNibName:@"TabBarButtonView" target:nil];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.isSelected = NO;
}
- (IBAction)onClick:(UIButton *)sender {
    if (_delegate && [_delegate respondsToSelector:@selector(tabBarItemOnClickWithSender:)]) {
        [_delegate tabBarItemOnClickWithSender:self];
    }
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    if (isSelected) {
        _label.textColor = [UIColor colorFromHexString:@"fc6721"];
        _label.layer.borderWidth = 3;
        if (_selectedString) {
            _label.text = _selectedString;
        }
    }else {
        _label.textColor = [UIColor whiteColor];
        _label.layer.borderWidth = 0;
        if (_normalString) {
            _label.text = _normalString;
        }
    }
}

@end
