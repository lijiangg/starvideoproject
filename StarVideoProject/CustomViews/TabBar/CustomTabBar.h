//
//  CustomTabBar.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/29.
//  Copyright © 2018年 star. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabBarButtonView.h"
NS_ASSUME_NONNULL_BEGIN

@protocol CustomTabBarDelegate <UITabBarDelegate>

- (void)tabBarSelectIndex:(NSInteger)index;

@end
@interface CustomTabBar : UITabBar <TabBarButtonViewDelegate>
@property (strong, nonatomic) IBOutlet UIView *view;

@property (weak, nonatomic)     id<CustomTabBarDelegate>customDelegate;
@end

NS_ASSUME_NONNULL_END
