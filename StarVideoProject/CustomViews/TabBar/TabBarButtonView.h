//
//  TabBarButtonView.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/27.
//  Copyright © 2018年 star. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TabBarButtonView;
NS_ASSUME_NONNULL_BEGIN

@protocol TabBarButtonViewDelegate <NSObject>

- (void)tabBarItemOnClickWithSender:(TabBarButtonView *)sender;

@end
@interface TabBarButtonView : UIView
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UILabel *label;

@property (copy, nonatomic)IBInspectable NSString *normalString;
@property (copy, nonatomic)IBInspectable NSString *selectedString;
@property (assign, nonatomic) BOOL isSelected;

@property (weak, nonatomic)IBOutlet id<TabBarButtonViewDelegate>delegate;

+ (TabBarButtonView *)bar;
@end

NS_ASSUME_NONNULL_END
