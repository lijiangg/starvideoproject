//
//  CustomTabBar.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/29.
//  Copyright © 2018年 star. All rights reserved.
//

#import "CustomTabBar.h"

#define tabBarViewTag 332244
@implementation CustomTabBar {
    NSArray *titleArray;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        titleArray = @[@"推荐",@"视频",@"音乐",@"图集",@"明星"];
        [self setSubviews];
    }
    return self;
}

- (void)setSubviews {
    for (int index = 0; index < 5; index ++) {
        TabBarButtonView *btn = [TabBarButtonView bar];
        btn.tag = tabBarViewTag + index;
        btn.delegate = self;
        btn.normalString = titleArray[index];
        if (index == 0) {
            btn.isSelected = YES;//默认第一个选中
        }else {
            btn.isSelected = NO;
        }
        [self addSubview:btn];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    // 遍历所有按钮, 调整按钮位置
    for (UIView *subView in self.subviews) {
        if ([subView isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            [subView removeFromSuperview];
        }
    }
    
    for (int index = 0 ; index < 5; index ++) {
        TabBarButtonView *btn = [self viewWithTag:tabBarViewTag + index];
        CGFloat width = self.bounds.size.width/5;
        CGFloat height = self.bounds.size.height;
        CGFloat x = self.bounds.size.width/5 * index;
        CGFloat y = 0;
        btn.frame = CGRectMake(x, y, width, height);
    }
    
}

- (void)tabBarItemOnClickWithSender:(TabBarButtonView *)sender {
    for (int index = 0; index < 5; index ++) {
        TabBarButtonView *barView = [self viewWithTag:tabBarViewTag + index];
        if ([barView isEqual:sender]) {
            barView.isSelected = YES;
            if (_customDelegate && [_customDelegate respondsToSelector:@selector(tabBarSelectIndex:)]) {
                [_customDelegate tabBarSelectIndex:index];
            }
        }else {
            barView.isSelected = NO;
        }
    }
}

@end
