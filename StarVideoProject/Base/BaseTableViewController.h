//
//  BaseTableViewController.h
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseTableViewController : UITableViewController

@end

NS_ASSUME_NONNULL_END
