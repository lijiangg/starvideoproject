//
//  BaseTableView.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/26.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseTableView.h"

@implementation BaseTableView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib {
    [super awakeFromNib];
    self.separatorStyle = UITableViewCellSeparatorStyleNone;
}

@end
