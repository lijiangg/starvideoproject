//
//  BaseNavigationController.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseNavigationController.h"

@interface BaseNavigationController ()

@end

@implementation BaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (UIStatusBarStyle)preferredStatusBarStyle

{
    
    UIViewController* topVC = self.topViewController;
    
    return [topVC preferredStatusBarStyle];
    
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
//    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [leftBtn setImage:[UIImage imageNamed:@"menu_white"] forState:UIControlStateNormal];
//    leftBtn.frame = CGRectMake(0, 0, 30, 30);
//    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
//    self.navigationItem.leftBarButtonItem = leftItem;
//
//    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [rightBtn setImage:[UIImage imageNamed:@"threePoint_white"] forState:UIControlStateNormal];
//    rightBtn.frame = CGRectMake(0, 0, 30, 30);
//    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
//    self.navigationItem.leftBarButtonItem = rightItem;
//
//    SearchBarView *searchView = [UINib nibWithNibName:@"SearchBarView" target:nil];
//    searchView.frame = CGRectMake(0, 0, 200, 40);
//    self.navigationItem.titleView = searchView;
    
    [super pushViewController:viewController
                     animated:animated];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
