//
//  BaseCollectionViewController.h
//  StarVideoProject
//
//  Created by lijiang on 2018/12/2.
//  Copyright © 2018年 star. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseCollectionViewController : UICollectionViewController

@end

NS_ASSUME_NONNULL_END
