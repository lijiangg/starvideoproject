//
//  BaseViewController.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseViewController.h"
#import "SearchBarView.h"
@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupNavigationBarItem];
}

- (void)setupNavigationBarItem {
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn setImage:[UIImage imageNamed:@"menu_white"] forState:UIControlStateNormal];
    leftBtn.frame = CGRectMake(0, 0, 30, 30);
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn setImage:[UIImage imageNamed:@"threePoint_white"] forState:UIControlStateNormal];
    rightBtn.frame = CGRectMake(0, 0, 30, 30);
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
    
    SearchBarView *searchView = [UINib nibWithNibName:@"SearchBarView" target:nil];
    searchView.frame = CGRectMake(0, 0, 200, 40);
    self.navigationItem.titleView = searchView;
   
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(UIStatusBarStyle)preferredStatusBarStyle
{
    //设置为白色
    return UIStatusBarStyleLightContent;
    //默认为黑色
    //    return UIStatusBarStyleDefault;
}
@end
