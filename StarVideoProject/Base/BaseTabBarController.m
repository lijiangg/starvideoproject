//
//  BaseTabBarController.m
//  StarVideoProject
//
//  Created by lijiang on 2018/11/25.
//  Copyright © 2018年 star. All rights reserved.
//

#import "BaseTabBarController.h"
#import "CustomTabBar.h"
@interface BaseTabBarController () <CustomTabBarDelegate>
@property (strong, nonatomic) CustomTabBar *customTabBar;
@end

@implementation BaseTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tabBar.hidden = YES;
    //加载 tabBar
    [self setValue:self.customTabBar forKey:@"tabBar"];

}

- (void)tabBarSelectIndex:(NSInteger)index {
    self.selectedIndex = index;
}
- (CustomTabBar *)customTabBar {
    if (!_customTabBar) {
        _customTabBar = [CustomTabBar new];
        _customTabBar.customDelegate = self;
    }
    return _customTabBar;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
