//
//  BaseTableViewCell.h
//  FTMS_MIVS
//
//  Created by lijiang on 2018/11/15.
//  Copyright © 2018年 yuanjingshidian. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseTableViewCell : UITableViewCell

@property (assign, nonatomic)IBInspectable BOOL isShowLine;
@end

NS_ASSUME_NONNULL_END
